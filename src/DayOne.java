/**
 * Emma Gurung
 * Day 1
 * Introduction to class, instance variables,
 * local variables, methods and constructors
 */
public class DayOne {

    // instance variables
    private int number;
    private String text;
    private boolean flag;
    private int z;

    DayOne(){ }

    public void firstMethodName(int x, String y) {
        // local variables
        int z;
        z = x;

        System.out.println("The number is " + x);
        System.out.println("The text is " + y);
        System.out.println("The uppercase form of text is " + y.toUpperCase());
        System.out.println("The uppercase form of text is " + y.toLowerCase());
        System.out.println("The length of the text is " + y.length());
        System.out.println(y.concat(y));
        System.out.println(x == 20);
        System.out.println(x != 20);
    }

    public void getVariables() {
        number = 10;
        System.out.println("Number is:" + number);

    }

    public static void main(String[] args) {
        DayOne d1 = new DayOne();
        d1.firstMethodName(20, "Twenty");
        d1.getVariables();
    }

}
