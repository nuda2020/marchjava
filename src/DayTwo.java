   /**
     * This class includes the default constructor,
     * constructor overloading and shows method overloading.
     * It also shows the use of if else and boolean cases.
     **/

    public class DayTwo {
        private int number1;
        public String message;
        private double number2;
        boolean isJava = true;

        // Constructor overloading part
        DayTwo() {
        }

        DayTwo(double y) {
        }

        DayTwo(int x, String text) {
        }

        // Method overloading part
        public void displays(int x1, int y1) {
            int z = x1 + y1;
            System.out.println("Sum is:" + z);
        }

        public void displays(String msg) {
            System.out.println(msg);
        }

        // Use of If else and elseif statements
        public void checkgrades(int total) {
            if (total >= 50 && total <= 100) {
                System.out.println("Congratulations! You passed.");
            } else if (total >= 100 || total < 0) {
                System.out.println("Invalid Total number");
            } else {
                System.out.println("Don't Give up! You need to work harder.");
            }
        }

        public void find(boolean answer) {
            if (answer) {
                System.out.println("The answer is True.");
            } else {
                System.out.println("The answer is false.");
            }
        }

        public static void main(String[] args) {

            // Creating instance with default constructor
            DayTwo d2 = new DayTwo(8.99);
            d2.displays("This is float");

            //Creating instance object with constructor overloading
            DayTwo d = new DayTwo(10, "Ten");
            System.out.println(d.isJava);
            d.displays(5, 9);
            d.displays("This is the method with parameters");

            d2.checkgrades(85);
            d2.checkgrades(40);
            d2.checkgrades(120);
            d2.checkgrades(-9);

            boolean isA = true;
            d2.find(isA);


        }


    }






    
